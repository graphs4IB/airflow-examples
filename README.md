# Airflow

Directed Acyclic Graphs (DAG) as used in Apache Airflow, leveraging Google Cloud Composer.

# Setup

## Create Environment

Essentially follow the instructions from Google's [Quickstart](https://cloud.google.com/composer/docs/quickstart).
Start by the "before you begin" instructions.

We will be using the [web console](https://console.cloud.google.com), in the `us-central1` location, using
the [Cloud Shell](https://cloud.google.com/shell/) as a convenient,
secure and inexpensive way to interact with GCP services.

We are creating the environment with
```
gcloud beta composer environments create viking \
    --location us-central1 \
    --zone us-central1-f \
    --machine-type g1-small \
    --disk-size=20 \
    --python-version=3
```

Set the environment variables, assuming that we have previous created the bucket `gs://vikings-wordcount`:
```
gcloud composer environments run viking --location us-central1 variables -- --set gcp_project ${GOOGLE_CLOUD_PROJECT}
gcloud composer environments run viking --location us-central1 variables -- --set gcs_bucket gs://vikings-wordcount
gcloud composer environments run viking --location us-central1 variables -- --set gce_zone us-central1-c
gcloud composer environments run viking --location us-central1 variables -- --get gcs_bucket
```

## Deploy dag

The vaguely modified quick start from Google is published in [the current repo](https://gitlab.com/graphs4IB/airflow-examples/tree/master/).

```
gcloud composer environments storage dags import --environment=viking --location us-central1 --source quickstart.py
```
